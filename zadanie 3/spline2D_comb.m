function spline2D_comb()
  
  
filename="babia.png";
inverse=false;

knot_width = 50
knot_height = 50 %%200

precision = 0.01 %0.005


knot_vectorx = [1, 1, 1, ]
knot_vectory = [1, 1, 1, ]

i=2
while ( i <= (knot_width-1) )
  knot_vectorx = [knot_vectorx, i];
  i = i+1
endwhile

i=2
while ( i <= (knot_height-1) )
  knot_vectory = [knot_vectory, i];
  i = i+1
endwhile

knot_vectorx = [knot_vectorx, knot_width-1];
knot_vectorx = [knot_vectorx, knot_width-1];

knot_vectory = [knot_vectory, knot_height-1];
knot_vectory = [knot_vectory, knot_height-1];


coeffs = zeros(knot_width, knot_height);


X = imread(filename);

%avgX = zeros(size(X)(1), size(X)(2));

%i = 1;
%while (i < size(X)(1))
%  j = 1;
%  while(j < size(X)(2))
%    avgX(i,j) = floor((X(i,j,1)+X(i,j,2)+X(i,j,3))/3);
%    j = j+1;
%  endwhile
%  i = i+1;
%endwhile

imgStepI = size(X)(1)/knot_width;
imgStepJ = size(X)(2)/knot_height;

i=1;
while ( i<knot_width)
  j=1;
  while(j < knot_height)
    imgI=floor(i*imgStepI);
    imgJ=floor(j*imgStepJ);
    coeffs(i,j) = (cast(X(imgI,imgJ,1),"double") + cast(X(imgI,imgJ,2),"double") +cast(X(imgI,imgJ,3), "double"))/3/255;
    if inverse
      coeffs(i,j) = 1-coeffs(i,j);
    endif
    j=j+1;
  endwhile
  i=i+1;
endwhile

disp(coeffs)




%knot_vectorx=[0 0 0 1 2 3 4 4 5 6 6 6] %9

%knot_vectory=[0 0 0 1 2 3 3 4 4 4] %7 
%k

%coeffs= [ 0.0   0.0   0.5   0.0   0.0   0.0   0;
%          0.0   0.0   0.5   0.0   0.0   0.0   0;
%          0.0   0.0   0.5   0.0   0.2   0.5   0;
%          0.0   0.0   0.5   0.0   0.5   0.0   0;
%          0.0   0.0   0.5   0.5   0.3   0.0   0;
%          0.0   0.0   0.5   0.0   0.5   0.0   0;
%          0.0   0.0   0.5   0.0   0.2   0.5   0;
%          0.0   0.0   0.5   0.0   0.0   0.0   0;
%          0.0   0.0   0.5   0.0   0.0   0.0   0];


%macros
compute_nr_basis_functions = @(knot_vector,p) size(knot_vector, 2) - p - 1
mesh = @(a,c) a:precision*(c-a):c

%splines in x
px = compute_p(knot_vectorx)
tx = check_sanity(knot_vectorx,px)
nrx = compute_nr_basis_functions(knot_vectorx,px)

x_begin = knot_vectorx(1)
x_end = knot_vectorx(nrx+px)

x=mesh(x_begin,x_end);

%splines in y
py = compute_p(knot_vectory)
ty = check_sanity(knot_vectory,py)
nry = compute_nr_basis_functions(knot_vectory,py)

y_begin = knot_vectory(1)
y_end = knot_vectory(nry+py)

y=mesh(y_begin,y_end);

%X and Y coordinates of points over the 2D mesh
[X,Y]=meshgrid(x,y);
hold on

result = zeros(size(X)(1), size(X)(2))
%nrx 7  nry 5
for i=1:nrx
%compute values of
vx=compute_spline(knot_vectorx,px,i,X);
for j=1:nry
vy=compute_spline(knot_vectory,py,j,Y);
result += vx.*vy * coeffs(i, j);
%surf(X,Y,vx.*vy);
endfor
endfor
surf(X,Y,result);
axis([0 knot_width 0 knot_height 0 2]);
hold off



% Subroutine computing order of polynomials
function p=compute_p(knot_vector)

% first entry in knot_vector
  initial = knot_vector(1);
% lenght of knot_vector
  kvsize = size(knot_vector,2);
  p = 0;

% checking number of repetitions of first entry in knot_vector
  while (p+2 <= kvsize) && (initial == knot_vector(p+2))
    p = p+1;
  end
  
  return
end

% Subroutine computing basis functions according to recursive Cox-de-Boor formulae
function y=compute_splines(knot_vector,p,nr,x)

y=compute_spline(knot_vector,p,nr,x);
%y=y.*sin(pi/10*x);
return
end

% Subroutine computing basis functions according to recursive Cox-de-Boor formulae
function y=compute_spline(knot_vector,p,nr,x)
  
% function (x-x_i)/(x_{i-p}-x_i)
  fC= @(x,a,b) (x-a)/(b-a);
% function (x_{i+p+1}-x)/(x_{i+p+1}-x_{i+1})
  fD= @(x,c,d) (d-x)/(d-c);
  
% x_i  
  a = knot_vector(nr);
% x_{i-p} 
  b = knot_vector(nr+p);
% x_{i+1}
  c = knot_vector(nr+1);
% x_{i+p+1}
  d = knot_vector(nr+p+1);

% linear function for p=0
  if (p==0)
    y = 0 .* (x < a) + 1 .* (a <= x & x <= d) + 0 .* (x > d);
    return
  end

% B_{i,p-1}  
  lp = compute_spline(knot_vector,p-1,nr,x);
% B_{i+1,p-1}
  rp = compute_spline(knot_vector,p-1,nr+1,x);
  
% (x-x_i)/(x_{i-p)-x_i)*B_{i,p-1}  
  if (a==b)
% if knots in knot_vector are repeated we have to include it in formula
    y1 = 0 .* (x < a) + 1 .* (a <= x & x <= b) + 0 .* (x > b);
  else
    y1 = 0 .* (x < a) + fC(x,a,b) .* (a <= x & x <= b) + 0 .* (x > b);
  end
  
% (x_{i+p+1}-x)/(x_{i+p+1)-x_{i+1})*B_{i+1,p-1}
  if (c==d)
% if knots in knot_vector are repeated we have to include it in formula
    y2 = 0 .* (x < c) + 1 .* (c < x & x <= d) + 0 .* (d < x);
  else
    y2 = 0 .* (x < c) + fD(x,c,d) .* (c < x & x <= d) + 0 .* (d < x);
  end
  
  y = lp .* y1 + rp .* y2;
  return
end


% Subroutine checking sanity of knot_vector
function t=check_sanity(knot_vector,p)

  initial = knot_vector(1);
  kvsize = size(knot_vector,2);

  t = true;
  counter = 1;

% if number of repeated knots at the beginning of knot_vector doesn't match polynomial order
  for i=1:p+1
    if (initial ~= knot_vector(i))
% return FALSE
      t = false;
      return
    end
  end

% if there are too many repeated knots in the middle of knot_vector
  for i=p+2:kvsize-p-1
    if (initial == knot_vector(i))
      counter = counter + 1;
      if (counter > p)
% return FALSE
        t = false;
        return
      end
    else
      initial = knot_vector(i);
      counter = 1;
    end
  end

  initial = knot_vector(kvsize);
  
% if number of repeated knots at the end of knot_vector doesn't match polynomial order
  for i=kvsize-p:kvsize
    if (initial ~= knot_vector(i))
% return FALSE
      t = false;
      return
    end
  end
  
% if subsequent element in knot_vector is smaller than previous one
  for i=1:kvsize-1
    if (knot_vector(i)>knot_vector(i+1))
% return FALSE
      t = false;
      return
    end
  end

  return

end



end
